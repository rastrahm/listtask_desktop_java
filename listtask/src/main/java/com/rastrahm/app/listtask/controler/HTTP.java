package com.rastrahm.app.listtask.controler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Clase que maneja las conecciones http
 * @author Rolando Strahm
 */
public class HTTP {
    /**
     * Método que genera un request según la URL a través de un GET
     * @param path : String Ruta a impactar
     * @param jwt  : String token con el JWT
     * @return String : String con el response del servidor
     */
    public static String sendGET(String path, String jwt) {
        try {
            URL url = new URL(path);
            HttpURLConnection host= (HttpURLConnection) url.openConnection();
            host.setRequestMethod("GET");
            host.setRequestProperty("Content-Type", "application/json; utf-8");
            host.setRequestProperty("Accept", "application/json");
            if (jwt != null) {
                host.setRequestProperty("jwt", jwt);
            }
            int responseCode = host.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) { 
                BufferedReader in = new BufferedReader(new InputStreamReader(host.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return response.toString();
            } else {
                return "GET no funciona";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "GET no funciona, URL mal formada " + e.getMessage().toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "GET no funciona, Execpci��n de entrada salida " + e.getMessage().toString();
        }
    }
    
    /**
     * Metodo que genera un request según el URL a través de un POST
     * @param path : String Ruta al servidro
     * @param content : String Ruta con el contenido
     * @param jwt : String con el token JWT
     * @return String : con el resultado de la conexión
     */
    public static String sendPOST(String path, String content, String jwt) {
        try {
            URL url = new URL(path);
            HttpURLConnection host= (HttpURLConnection) url.openConnection();
            host.setDoOutput(true);
            host.setRequestMethod("POST");
            host.setRequestProperty("Content-Type", "application/json; utf-8");
            host.setRequestProperty("Accept", "application/json");
            if (jwt != null) {
                host.setRequestProperty("jwt", jwt);
            }
            try(OutputStream os = host.getOutputStream()) {
                byte[] input = content.getBytes("utf-8");
                os.write(input, 0, input.length);			
            }
            int responseCode = host.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) { 
                BufferedReader in = new BufferedReader(new InputStreamReader(host.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return response.toString();
            } else {
                return "GET no funciona";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "GET no funciona, URL mal formada " + e.getMessage().toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "GET no funciona, Execpci��n de entrada salida " + e.getMessage().toString();
        }
    }
	
}
