/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rastrahm.app.listtask.model;

/**
 * Clase que detemina si se refrescan los objetos graficos
 * @author Rolando Strahm
 */
public class AppStatus {
    
    public static boolean refresh;

    /**
     * Constructor de clase
     */
    public AppStatus() {
    }

    /**
     * Devuelve el valor de la propiedad
     * @return boolean : true refrescar, false no refrescar
     */
    public static boolean isRefresh() {
        return refresh;
    }

    /**
     * Coloca el valor de la propiedad
     * @param refresh : boolean true refrescar, false no refrescar
     */
    public static void setRefresh(boolean refresh) {
        AppStatus.refresh = refresh;
    }
}
