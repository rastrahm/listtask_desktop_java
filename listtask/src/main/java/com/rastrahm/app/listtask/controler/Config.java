package com.rastrahm.app.listtask.controler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Se lee los archivos de configuración
 * @author Rolando Strahm
 */

public class Config {
    Properties configProp;
    /**
    * Objeto que lee el archivo de configuración, lee el archivo database.properties y retorna las propiedades, para ubicar dicho archivo
    * se utiliza la propiedad catalina.base del objeto System mas la ruta de las propiedades.
    * Al instanciar el objeto automáticamente apertura el archivo para su lectura
    * @param path: String con la ruta y nombre del archivo de configuración
    */
    public Config(String path) {
        configProp = new java.util.Properties();
        try {
            configProp.load(this.getClass().getClassLoader().getResourceAsStream(path));
        } catch(Exception e) {
            File currentDirFile = new File(".");
            String currentDir = System.getProperty("user.dir");
            File configFile = new File(currentDir + "/" + path);
            Properties Config = new Properties();
            if (configFile.exists()) {
                InputStream stream;
		try {
                    stream = new FileInputStream(configFile);
                    configProp.load(stream);
		} catch (IOException e1) {
                    e1.printStackTrace();
		}
            }
            e.printStackTrace();
        }
    }
	
   /**
    * Retorna la propiedad solicitada
    * @param key : Propiedad solicitada
    * @return String : Contenido de la propiedad
    */
    public String getProperty(String key) {
        String value = this.configProp.getProperty(key);
        return value;
    }
    
    /**
     * Coloca una propiedad em un key para guardarlo
     * @param key : String que contiene la clave a guardar
     * @param value : String con el valor de la clave a gruardar
     */
    public void setProperty(String key, String value) {
        this.configProp.setProperty(key, value);
    }
    
    /**
     * Guarda las propiedades
     * @param path : String con la ruta del archivo de propiedades
     */
    public void saveProperty(String path) {
        try {
            configProp.store(new FileOutputStream(path), null);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
